import React from 'react';
import './App.scss';
import Navigation from "./navigation/Navigation";
import Footer from "./footer/Footer";
import Landing from "./landing/Landing";
import history from './history';
import {Route, Router} from "react-router";

function App() {
    return (
        <Router history={history}>
            <div className="App">
                <Navigation/>

                <Route path="/" component={Landing}/>

                <Footer/>
            </div>
        </Router>
    );
}

export default App;
