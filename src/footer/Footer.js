import React from "react";
import "./Footer.scss";

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <p>
          Copyright © 2018 | Designed by <a className="text-white-50" href="http://vrockai.github.io">vrockai</a>
        </p>
      </footer>
    );
  }
}

export default Footer;
