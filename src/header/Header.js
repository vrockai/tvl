import React from "react";
import './Header.scss';

class Header extends React.Component {
  render() {
    return (
      <header className="masthead text-center text-white d-flex">
        <div className="container my-auto">
          <div className="row">
            <div className="col-lg-10 mx-auto">
              <h1 className="text-uppercase">
                <strong>Your Favorite Way to Learn English</strong>
              </h1>
              <hr/>
            </div>
            <div className="col-lg-8 mx-auto">
              <p className="text-faded mb-5">
                Binge watching never felt so useful! Learn vocabulary as you watch
                your favorite series or movies. OMG! So cool! And it's free!
              </p>
              <p>It just doesn't exist yet</p>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
