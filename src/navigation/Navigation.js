import React from "react";
import "./Navigation.scss";
import { withRouter } from "react-router";
import { Link } from 'react-router-dom';

class Navigation extends React.Component {
  goTo(route) {
    this.props.history.replace(`/${route}`)
  }

  render() {
    return (
      <nav
        className={"navbar navbar-expand-lg navbar-light " + (this.props.location.pathname === '/' ? 'fixed-top' : '')}
        id="mainNav">
        <div className="container">
          <Link className="navbar-brand js-scroll-trigger"
                to="/"
                onClick={this.goTo.bind(this, 'home')}>
            TVL
          </Link>
        </div>
      </nav>
    );
  }
}

export default withRouter(Navigation);
